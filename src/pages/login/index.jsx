import Mac from '../../assets/images/Mac.png'
import Icon1 from '../../assets/images/input-ico1.png'
import Icon2 from '../../assets/images/input-ico2.png'
import Icon3 from '../../assets/images/input-ico3.png'

export default function LoginPage () {
    return (
        <div className='flex justify-center'>
            <div className='w-1/2 flex flex-col justify-center alignn-middle h-screen bg-gradient-radial from-blue-400 via-blue-500 to-blue-700'>
                <h1 className='flex flex-start text-white text-5xl font-bold ml-20 mt-20'>Bootcamp</h1>
                <div className='flex justify-center mr-20'>
                    <img className='m-48' src={Mac} alt='mac'/>
                </div>
            </div>
            
            <div className='w-1/2 h-screen relative'>
                <form className='w-5/6 m-auto flex flex-col text-left mt-40'>
                    <h1 className='text-4xl mb-16 font-bold '>Вход в платформу</h1>

                    <label className='block text-grey-darker text-sm font-bold mb-2 text-base' htmlFor="login">Имя пользователья <span className='text-red-500'>*</span></label>
                    <div className='relative mb-5'>
                        <img className='absolute top-5 left-5 ' src={Icon1} alt='icon1' />
                        <input className='w-full border rounded py-3 px-14' type="text" id='login' placeholder='Арина Соколова' />
                    </div>

                    <label className='block text-grey-darker text-sm font-bold mb-2 text-base' htmlFor="password">Пароль <span className='text-red-500'>*</span></label>
                    <div className='relative mb-8'>
                        <img className='absolute top-4 left-5' src={Icon2} alt='icon2' />
                        <input className='w-full border rounded py-3 px-14' type="password" id='password' placeholder='Введите пароль' />
                        <img className='absolute top-5 right-5' src={Icon3} alt='icon3' />
                    </div>

                    <div className='flex flex-row mb-10'>
                        <input className='w-6 h-6 mr-4' type="checkbox" id='checkbox' />
                        <label className='text-grey-400' htmlFor="checkbox">Запомнить меня</label>
                    </div>

                    <button className='rounded-xl text-white bg-blue-600 py-4'>Войти</button>
                </form>
                    <p className='absolute bottom-10 left-10 text-gray-400'>Copyright ©Girgitton. Все права защищены</p>
            </div>
        </div>
    )
}
