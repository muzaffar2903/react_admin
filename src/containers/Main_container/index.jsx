import { Outlet } from 'react-router-dom'

// import Navbar from '@layouts/Navbar'
// import Sidebar from '@layouts/Sidebar'
// import Wrapper from '@layouts/Wrapper'

export default function MainContainer () {
    return (
        <div className="flex bg-white">
            <div className="flex-none w-1/5 bg-sky-400 h-full">
                <div className="fixed bg-sky-500 h-screen flex w-1/5">Sidebar</div>
            </div>
            <div className="shrink w-full h-full">
                <div className="flex-none bg-orange-50">{ <Outlet/>}</div>
            </div>
        </div>
    )
}
