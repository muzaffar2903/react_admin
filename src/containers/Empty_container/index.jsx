import { Outlet } from "react-router-dom";

export default function EmptyContainer () {
    return (
        <div>
            <div>{<Outlet />}</div>
        </div>
    )
}